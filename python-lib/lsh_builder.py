import dataiku
import numpy as np
import pandas as pd
import itertools
import logging
from scipy.spatial.distance import pdist
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer

VALID_COLUMN_TYPES = ['tinyint', 'smallint', 'int', 'bigint', 'float', 'double']
DISTANCE_METRICS = ['euclidean', 'cosine', 'correlation', 'chebyshev', 'canberra', 'braycurtis']

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

class RecipeParams(object):
    def __init__(self, params, input_dataset, out_dataset):
        self.input_dataset      = str(input_dataset)
        self.out_dataset        = str(out_dataset)
        self.primary_key_column = str(params['primary_key_column'])
        self.norm_and_scale     = params['norm_and_scale']
        self.compute_distances  = params['compute_distances']
        self.feature_columns    = [str(c) for c in params['feature_columns']]
        self.projection_length  = int(params['projection_length'])
        self.input_columns      = self.generate_input_columns()
        self.projection_seed    = int(params['projection_seed'])

    def validate(self):
        """
        check if no input or output dataset has been selected (theoretically not possible, but still)
        check if primary key part of the input schema
        check if feature columns of allowed type
        """
        if self.input_dataset == None or self.input_dataset == '':
            raise ValueError("Invalid input dataset provided")
        elif self.out_dataset == None or self.out_dataset == '':
            raise ValueError("Invalid output dataset provided")

        input_dataset_schema = dataiku.Dataset(self.input_dataset).read_schema()
        columns = [c['name'] for c in input_dataset_schema]
        if self.primary_key_column not in columns:
          raise ValueError("Selected index column is not in the dataset")

        for column in input_dataset_schema:
          if column['name'] in self.feature_columns and column['type'] not in VALID_COLUMN_TYPES:
            raise ValueError("Column {} is not of an valid type to build LSH, please exclude from the list of feature columns".format(column['name']))

        return self

    def generate_input_columns(self):
      if len(self.feature_columns)==0:
        input_dataset_schema = dataiku.Dataset(self.input_dataset).read_schema()
        input_columns = [c['name'] for c in input_dataset_schema]
      else:
        input_columns = list(self.feature_columns)
        input_columns.insert(0, str(self.primary_key_column))
      return input_columns

class LshBuilder(object):
    def __init__(self, params):
        self.params = params

    def build(self):
        self.read_input_data()
        self.normalise_dataset()
        self.perform_projection_clustering()
        self.calculate_in_cluster_distances()

    def read_input_data(self):
        """
        read using the DSS API. This could be chunked(?)
        """
        df = dataiku.Dataset(self.params.input_dataset).get_dataframe(columns=self.params.input_columns)
        df = df.set_index(self.params.primary_key_column)
        self.data = df.values
        self.index = df.index.values
        del df
        return self

    def normalise_dataset(self):
        """
        perform the optional normalisation and scaling
        """
        if self.params.norm_and_scale==True:
          self.data = StandardScaler().fit_transform(self.data)
          self.data = Normalizer().fit_transform(self.data)
        return self

    def perform_projection_clustering(self):
        """
        perform the hashing function as dot product to each projection vector
        convert booleans into int, then strings, concatenate the string to created the cluster name
        put the clusters into a dataframe which will be used for filtering and iteration later on.
        """
        clusters = []
        np.random.seed(seed=self.params.projection_seed)
        projections = np.random.randn(self.params.projection_length, self.data.shape[1])

        lsh = (np.dot(self.data, projections.T) > 0)
        lsh = lsh.astype('int').astype('str')

        for i in range(lsh.shape[0]):
            clusters.append(''.join(lsh[i,:]))
        df = pd.DataFrame(clusters, columns=['cluster_id'])

        self.clusters = df
        del lsh, df
        return self

    def calculate_in_cluster_distances(self):
        """
        this method outputs either rich results or just clusters, based on the chosen parameters.
        if just clusters, then the index_column along with the cluster will be outputed.
        if distances are selected, then we iterate over each of the clusters, calculate the distances
        and then write the distances (user_1 to user_2 and user_2 to user_1), along with the clusters themselves.
        """
        out_dataset   = dataiku.Dataset(self.params.out_dataset)
        column_name   = self.params.primary_key_column

        if self.params.compute_distances==True:
          clusters_uniq = self.clusters.cluster_id.unique()
          out_schema    = generate_output_schema(column_name)
          out_dataset.write_schema(out_schema)

          with out_dataset.get_writer() as writer:
            for ix_c, cluster in enumerate(clusters_uniq):
              query      = "cluster_id == '{}'".format(cluster)
              data_index = self.clusters.query(query).index
              columns    = self.index[data_index]
              data       = self.data[data_index]
              distances  = {}

              for ix, d in enumerate(DISTANCE_METRICS):
                distance = '{}_dist'.format(d)
                dim_1    = '{}_1'.format(column_name)
                dim_2    = '{}_2'.format(column_name)
                try:
                  dist_mat = pdist(data, d)
                except MemoryError as exception:
                  raise MemoryError("Calculating distances requires more memory than is available on your machine. Please increase the number of features or size of the projection used to generate smaller clusters.")

                dist_df  = spacify_matrix(dist_mat,
                                          columns,
                                          cluster,
                                          [dim_1, dim_2, 'cluster_id', distance])

                # Here we want to create a dictionary of lists to consolidate at the end as
                # a single dataframe. The keys are dynamically added to the dict.
                if ix==0:
                  dim_1_data  = dist_df[dim_1]
                  dim_2_data  = dist_df[dim_2]
                  cluster_ids = dist_df['cluster_id']

                distances[distance] = dist_df[distance]

              dist_dict = {
                  dim_1: dim_1_data,
                  dim_2: dim_2_data,
                  'cluster_id': cluster_ids
              }
              for d in DISTANCE_METRICS:
                dist_dict['{}_dist'.format(d)] = distances.get('{}_dist'.format(d))

              df = pd.DataFrame(dist_dict)
              writer.write_dataframe(df)

        else:
          df = self.clusters.copy(deep=True)
          df[column_name] = self.index
          out_dataset.write_with_schema(df)

        return self

def spacify_matrix(distances, indices, cluster_id, columns):
    """
    here is important to verify the dimensions of the expected combinations
    and the matrix being passed actually can be unfolded.
    for each of the combinations look up the respective value in the matrix
    and create elements for each combination.
    """
    dim_1_list, dim_2_list = [], []
    combos = list(itertools.combinations(indices,2))

    if len(combos)!= distances.shape[0]:
      logger = logging.getLogger(__name__)
      logging.error('Dimensions mismatch!')

    for d in combos:
        dim_1_list.append(d[0])
        dim_2_list.append(d[1])

    data = {
        columns[0]: dim_1_list + dim_2_list,
        columns[1]: dim_2_list + dim_1_list,
        columns[2]: [cluster_id]*len(combos)*2,
        columns[3]: np.tile(distances, 2)
    }
    sparce_df = pd.DataFrame(data)

    return sparce_df

def generate_output_schema(column_name):
  out_schema = [{'name': '{}_1'.format(column_name), 'type': 'string'}]
  out_schema += [{'name': '{}_2'.format(column_name), 'type': 'string'}]
  for d in DISTANCE_METRICS:
    out_schema += [{'name': '{}_dist'.format(d), 'type': 'double'}]
  return out_schema
