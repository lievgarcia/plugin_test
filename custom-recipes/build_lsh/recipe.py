import dataiku
from dataiku.customrecipe import *
from dataiku import pandasutils as pdu
from lsh_builder import LshBuilder, RecipeParams


input_dataset  = get_input_names()[0]
output_dataset = get_output_names()[0]
recipe_params  = get_recipe_config()

params = RecipeParams(recipe_params, input_dataset, output_dataset)
params.validate()

lsh_builder = LshBuilder(params)
lsh_builder.build()